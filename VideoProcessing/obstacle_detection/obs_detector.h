#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"
#include <iostream>

using namespace cv;
using namespace std;

class ObstacleDetector {
	private:
		bool isTest;
		int dx[4] = {1, 0, -1, 0};
		int dy[4] = {0, +1, 0, -1};
		int **label;
		void dfs(int x, int y, int current_label, const Mat& u_disparity, int *label[]);
		vector<vector<int>> connected_labelling(const Mat& roi_u_disparity, const Mat& disparity);
		
	
	public:
		vector<vector<int>> detect(const Mat& disparity);
		ObstacleDetector(bool isTest = false);
};
