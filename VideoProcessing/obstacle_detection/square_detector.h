#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"


#include <iostream>
#include <math.h>
#include <string.h>

using namespace cv;
using namespace std;

class SquareDetector {
	public:
		bool isTest;
		SquareDetector(bool isTest = false);
		void findSquares( const Mat& image, vector<vector<Point> >& squares );
		void drawSquares( Mat& image, const vector<vector<Point> >& squares );
	private:
		int thresh = 100;
		int N = 11;
		double angle(const Point &pt1, const Point &pt2, const Point &pt0 );
		
};
