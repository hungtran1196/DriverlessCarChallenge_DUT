#include "obs_detector.h"

ObstacleDetector::ObstacleDetector(bool isTest) {
	this->isTest = isTest;
}

vector<vector<int>> ObstacleDetector::detect(const Mat& disparity) {

	Rect roi(0, 270, 640, 210);
	Mat temp = disparity(roi);

	double min, max;
	minMaxLoc(temp, &min, &max);
	if (!max) {
		vector<vector<int>> empt;
		return empt;
	}
	Mat u_disparity(max, temp.cols, CV_32S);
	Mat temp1(1, temp.cols, CV_8U);
	Mat copy;	
	for (int i = 1; i <= max; i ++) {
		compare(temp, i, temp1, CMP_EQ);
		reduce(temp1, copy, 0, CV_REDUCE_SUM, CV_32SC1);

		Mat r1 = u_disparity.row(i-1);
		copy.row(0).copyTo(r1);
	}	
	Mat dst, ud_8;
	u_disparity.convertTo(ud_8, CV_8U);

	adaptiveThreshold(ud_8, dst, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 3, 0);

	if (isTest) {
		imshow("roi_disparity", temp);
		imshow("roi_u_diparity", dst);
	}

	return connected_labelling(ud_8, temp);
	
}

vector<vector<int>> ObstacleDetector::connected_labelling(const Mat& udisparity, const Mat& disparity) {
	int row_count = udisparity.rows;
	int col_count = udisparity.cols;


	int *label[row_count];
	
	for (int i = 0; i < row_count; i++) {
		label[i] = new int[col_count];
		for (int j = 0; j < col_count; j++) label[i][j] = 0;	
	}


	int component = 0;
	
	for (int i = 0; i < udisparity.rows; i++) {
		for (int j = 0; j < udisparity.cols; j++) {
			if (!label[i][j] && int(udisparity.at<unsigned char>(i, j))) dfs(i,j, ++component, udisparity, label);
		}
	}

	int max_x[component+1];
	int min_x[component+1];
	int max_disp[component+1];
	int min_disp[component+1];

	for (int i = 1; i <= component; i++) {
		max_x[i] = -1;
		min_x[i] = -1;
		max_disp[i] = -1;
		min_disp[i] = -1;
	}

	for (int j = 0; j < col_count; j++) {
		for (int i = 0; i < row_count; i++) {
			if (label[i][j]) {
				int index = label[i][j];
				if (max_x[index] < j) max_x[index] = j;
				if (min_x[index] > j || min_x[index] == -1) min_x[index] = j;
				if (max_disp[index] < i) max_disp[index] = i;
				if (min_disp[index] > i || min_disp[index] == -1) min_disp[index] = i;
			}		
		}
	}

	vector<vector<int>> rs;

	for (int index = 1; index <= component; index++) {
		int max_j = max_x[index];
		int min_j = min_x[index];
		int max_d = max_disp[index];
		int min_d = min_disp[index];
		int max_i = -1;
		int min_i = -1;

		if (max_j - min_j < 50) continue;
		if (max_d  - min_d > 15) continue;
		cout << max_d << " " <<  min_d << endl;
		for (int j = min_j; j <= max_j; j++) {
			for (int i = 0; i < disparity.rows; i++) {
				int value = int(disparity.at<unsigned char>(i, j));
				//cout << value << " " << min_d << " " << max_d << endl;
				if (value >= min_d && value <= max_d) {
					if (i > max_i) max_i = i;
					if (min_i == -1||i < min_i) min_i = i;
				}

			}
		}

		vector<int> point;
		point.push_back(min_i);
		point.push_back(max_i);
		point.push_back(min_j);
		point.push_back(max_j);
		rs.push_back(point);
		
	}




	return rs;

}

void ObstacleDetector::dfs(int x, int y, int current_label, const Mat& img, int *label[]) {
	if (x < 0 || x == img.rows) return; // out of bounds
	if (y < 0 || y == img.cols) return; // out of bounds
	//cout << int (img.at<unsigned char>(x, y)) << endl;
	if (label[x][y] || !int (img.at<unsigned char>(x, y))) return; //already labelled or not marked with 1
	
	label[x][y] = current_label;

	  for (int direction = 0; direction < 4; ++direction)
    		dfs(x + dx[direction], y + dy[direction], current_label, img, label);

	
}
