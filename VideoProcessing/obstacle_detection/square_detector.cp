
SquareDetector::SquareDetector(bool isTest) {
	this->isTest = isTest;
}

void SquareDetector::findSquares( const Mat& image, vector<vector<Point> >& squares ) {
	squares.clear();
    	Mat mask, mask_blue, mask_green, mask_red_low, mask_red_high, hsv, temp, gray;
    	vector<vector<Point> > contours;
 	Mat pyr, timg;
	pyrDown(image, pyr, Size(image.cols/2, image.rows/2));
	pyrUp(pyr, timg, image.size());


	cvtColor(timg, hsv, CV_BGR2HSV);

	inRange(hsv, Scalar(100, 120, 100), Scalar(120, 255, 255), mask_blue);
	inRange(hsv, Scalar(41, 54, 63), Scalar( 71, 255, 255), mask_green);
	inRange(hsv, Scalar(0, 100,  100), Scalar( 15, 255, 255), mask_red_low);
	inRange(hsv, Scalar(160, 100, 120), Scalar( 15, 255, 255), mask_red_high);
	

	mask = mask_blue + mask_green + mask_red_low + mask_red_high;

	GaussianBlur(mask, temp, Size(11,11), 0);

	//bilateralFilter(temp, temp, 4, 50, 50);

	Canny(temp, gray, 0, 50, 5);
	dilate(gray, gray, Mat(), Point(-1,-1));

	if (isTest) imshow("Canny", gray);
		
	findContours(gray, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);
	vector<Point> approx;

	


	for( size_t i = 0; i < contours.size(); i++ )
            {
                // approximate contour with accuracy proportional
                // to the contour perimeter
            	approxPolyDP(Mat(contours[i]), approx, arcLength(Mat(contours[i]), true)*0.04, true);

                // square contours should have 4 vertices after approximation
                // relatively large area (to filter out noisy contours)
                // and be convex.
                // Note: absolute value of an area is used because
                // area may be positive or negative - in accordance with the
                // contour orientation
                if( approx.size() == 4 &&
                    fabs(contourArea(Mat(approx))) > 1000 &&
                    isContourConvex(Mat(approx)) )
                {
                    double maxCosine = 0;

                    for( int j = 2; j < 5; j++ )
                    {
                        // find the maximum cosine of the angle between joint edges
                        double cosine = fabs(angle(approx[j%4], approx[j-2], approx[j-1]));
                        maxCosine = MAX(maxCosine, cosine);
                    }

                    // if cosines of all angles are small
                    // (all angles are ~90 degree) then write quandrange
                    // vertices to resultant sequence
                    if( maxCosine < 0.3 )
                        squares.push_back(approx);
                }
            }

}

double SquareDetector::angle(const Point &pt1, const Point &pt2, const Point &pt0 ) {
	    double dx1 = pt1.x - pt0.x;
	    double dy1 = pt1.y - pt0.y;
	    double dx2 = pt2.x - pt0.x;
	    double dy2 = pt2.y - pt0.y;
	    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}
