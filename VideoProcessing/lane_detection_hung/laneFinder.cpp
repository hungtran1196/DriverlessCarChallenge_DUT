#include "laneFinder.h"


LaneFinder::LaneFinder(bool isTest) {
	this->transformed_h = 150;
	this->transformed_w = 150;
	this->isTest = isTest;
}

LaneFinder::~LaneFinder() { 
}

void LaneFinder::init() {
	this->rightLane = new Lane(false, this->transformed_h, this->transformed_w);
	this->leftLane = new Lane(true, this->transformed_h, this->transformed_w);
	this->_get_perspective_transform_ver2();
}

void LaneFinder::_get_perspective_transform_ver1() {



	Point2f inputQuad[4];
	Point2f outputQuad[4];

	inputQuad[0] = Point2f(40, 300);
	inputQuad[1] = Point2f(600, 300);
	inputQuad[2] = Point2f(-677, 560);
	inputQuad[3] = Point2f(1295, 560);


	outputQuad[0] = Point2f(0, 0);
	outputQuad[1] = Point2f(this->transformed_w, 0);
	outputQuad[2] = Point2f(0, this->transformed_h + 15);
	outputQuad[3] = Point2f(this->transformed_w, this->transformed_h + 15);

	this->M = getPerspectiveTransform(inputQuad, outputQuad);
	this->invM = getPerspectiveTransform(outputQuad, inputQuad); 
}

void LaneFinder::_get_perspective_transform_ver2() {

	this->transformed_h = 150;
	this->transformed_w = 150;

	Point2f inputQuad[4];
	Point2f outputQuad[4];

	inputQuad[0] = Point2f(0, 0);
	inputQuad[1] = Point2f(640, 0);
	inputQuad[2] = Point2f(-777, 300);
	inputQuad[3] = Point2f(1395, 300);


	outputQuad[0] = Point2f(0, 0);
	outputQuad[1] = Point2f(this->transformed_w, 0);
	outputQuad[2] = Point2f(0, this->transformed_h + 35);
	outputQuad[3] = Point2f(this->transformed_w, this->transformed_h + 35);

	this->M = getPerspectiveTransform(inputQuad, outputQuad);
	this->invM = getPerspectiveTransform(outputQuad, inputQuad); 
}

double LaneFinder::findTheta(const Mat& img, bool is_turn_left, bool is_turn_right, int *is_obs_left, int *is_obs_right, const vector<vector<Point>> &squares) {
	if (img.empty()) return 0;
	cout << "Begin" << endl;

	Mat transformed, binary, polynomial;
	auto bt = chrono::high_resolution_clock::now();


	//bt = chrono::high_resolution_clock::now();
	//this->_binary_image_before_ver1(img, binary);
	//printf("binary before Run in %.2fms \n", chrono::duration<double, milli> (chrono::high_resolution_clock::now() - bt).count());
	

//<-=======================Transform and convert image========================->

/*
	bt = chrono::high_resolution_clock::now();

	this->_transform_perspective(img, transformed);
	this->_binary_image_after_ver1(transformed, binary);

	printf("binary Run in %.2fms \n", chrono::duration<double, milli> (chrono::high_resolution_clock::now() - bt).count());

*/

	bt = chrono::high_resolution_clock::now();
	this->_binary_image_before_ver1(img, transformed);	
	this->_transform_perspective_ver2(transformed, binary);

	printf("binary Run in %.2fms \n", chrono::duration<double, milli> (chrono::high_resolution_clock::now() - bt).count());

//<-=======================For testing=======================================->
	if (this->isTest) {
		imshow("Color", img);
		imshow("Perspective", transformed);
		imshow("Binary", binary);
		polynomial = Mat::zeros(binary.size(), CV_8UC1);
	}

//<-======================Find left lane and right lane==========================->
	bt = chrono::high_resolution_clock::now();
	cout<<"Found right"<<endl;
	this->rightLane->find(binary);
	cout << "Done found right" << endl;

	cout<<"Found left"<<endl;
	this->leftLane->find(binary);


	//std::thread first(&Lane::find,this->rightLane, binary);
	//std::thread second(&Lane::find, this->leftLane, binary);

	//first.join();
	//second.join();

	printf("Find lane Run in %.2fms \n", chrono::duration<double, milli> (chrono::high_resolution_clock::now() - bt).count());



	bool right_empty = this->rightLane->best.empty();
	bool left_empty = this->leftLane->best.empty();
	int offset = 0;
	cout << "Done find " << right_empty <<left_empty << endl;

	if (right_empty && left_empty) return this->old_theta;
	cout << is_turn_left << " " << is_turn_right << endl;


//<-=======================For testing=======================================->
	if (this->isTest) {

		if (!right_empty)
			LaneFinder::_draw_poly(polynomial, this->rightLane->best);
		
		if (!left_empty)
			LaneFinder::_draw_poly(polynomial, this->leftLane->best);


		imshow("Center", polynomial);
	}


	if (squares.size()) {
		/* 0---------------1
		   |		   |
		   |  		   |			
		   2---------------3
		*/
		double cut_off = 480 - 200;
		for (int i = 0; i < squares.size(); i++) {
			vector<Point> square = squares[i];
			int i_left = 0, i_right = 0;	
			double y_left = square[i_left].y, y_right = square[i_right].y;
			for (int i = 1; i < 4; i++) {
				if (square[i].y > y_left) {
					y_left = square[i].y;
					i_left = i;
				}
			}

			for (int i = 1; i < 4; i++) {
				if (i== i_left) continue;
				if (square[i].y > y_right) {
					y_right = square[i].y;
					i_right = i;
				}
			}
			if (square[i_left].x > square[i_right].x) {
				int temp = i_right;
				i_right = i_left;
				i_left = temp;
			}

			int i_temp = 0;

			for (; i_temp < 4; i_temp++) {
				if (i_temp != i_right && i_temp != i_left) break;
			}
						

			double width = square[i_right].x - square[i_left].x; 
			double height = abs(square[i_right].y - square[i_temp].y);


			

			if (width < 60 || height < 60) continue;
			cout <<"Square, width: " << width << ", height: " << height << endl;

			if (!left_empty && !right_empty) {
				double offset = 0;
				bool isAbnormal = false;
				double theta = 0;
				_abnormal_line(offset, &theta, &isAbnormal);
				if (isAbnormal) {
					if (*is_obs_left) (*is_obs_left)++;
					if (*is_obs_right) (*is_obs_right) ++;
					cout << *is_obs_left << " " << *is_obs_right << endl;
					return 1.5 * theta;
				}

			}

			double bottom_square_left_x = (square[i_left].x * M.at<double>(0, 0) + (square[i_left].y-cut_off) * M.at<double>(0, 1) + M.at<double>(0, 2))
						/ (square[i_left].x * M.at<double>(2,0) + (square[i_left].y-cut_off) * M.at<double>(2, 1) + M.at<double>(2, 2));
			double bottom_square_right_x = (square[i_right].x * M.at<double>(0, 0) + (square[i_right].y-cut_off) * M.at<double>(0, 1) + M.at<double>(0, 2)) 
						/ (square[i_right].x * M.at<double>(2,0) + (square[i_right].y-cut_off) * M.at<double>(2, 1) + M.at<double>(2, 2));

			double bottom_square_center_x = (bottom_square_left_x + bottom_square_right_x) /2;

			double bottom_square_left_y = (square[i_left].x * M.at<double>(1, 0) + (square[i_left].y-cut_off) * M.at<double>(1, 1) + M.at<double>(1, 2))
						/ (square[i_left].x * M.at<double>(2,0) + (square[i_left].y-cut_off) * M.at<double>(2, 1) + M.at<double>(2, 2));

			double bottom_square_right_y = square[i_right].x * M.at<double>(1, 0) + (square[i_right].y-cut_off) * M.at<double>(1, 1) + M.at<double>(1, 2)
						/ (square[i_right].x * M.at<double>(2,0) + (square[i_right].y-cut_off) * M.at<double>(2, 1) + M.at<double>(2, 2));


			cout << "bottom left: " << bottom_square_left_x << "bottom right: " <<  bottom_square_right_x << endl;

			double bottom_square_y = max(abs(bottom_square_left_y), abs(bottom_square_right_y));

			
			double bottom_lane_left_x = 150/4, bottom_lane_right_x = 150/2 + 150/4;

			if (!left_empty) bottom_lane_left_x = bottom_square_y * bottom_square_y * this->leftLane->best.at<float>(0, 2) +
							bottom_square_y * this->leftLane->best.at<float>(0, 1) +
							this->leftLane->best.at<float>(0, 0);

			if (!right_empty) bottom_lane_right_x = bottom_square_y * bottom_square_y * this->rightLane->best.at<float>(0, 2) +
							bottom_square_y * this->rightLane->best.at<float>(0, 1) +
							this->rightLane->best.at<float>(0, 0);

			if (abs(bottom_square_center_x - bottom_lane_left_x) < abs(bottom_square_center_x - bottom_lane_right_x)) {
				//Obstacle on the left
				cout << *is_obs_left << " " << *is_obs_right << endl;
				*is_obs_left += 2;
				if (*is_obs_left >= 10) *is_obs_left = 10;
				if (*is_obs_left - *is_obs_right > 1) {
					cout << "obstacle on the left" << endl;

					if (!right_empty) return  2 * this->_find_theta(-bottom_lane_right_x + 150/2, bottom_square_y);
					else return -200;
				} else {
					*is_obs_right-=2;
					*is_obs_left-=2;
					if (*is_obs_right < 0) *is_obs_right = 0;
					if (*is_obs_left < 0) *is_obs_left = 0;
					return 2 * this->old_theta;
				}
				 
			} else {
				//Obstacle on the right	
					cout << *is_obs_left << " " << *is_obs_right << endl;			
				*is_obs_right += 2;
				if (*is_obs_right >= 10) *is_obs_right = 10;
				if (*is_obs_right - *is_obs_left > 1) {
					cout << "obstacle on the right" << endl;
					if (!left_empty) return 2 * this->_find_theta(-bottom_lane_left_x + 150/2, bottom_square_y);
					else return 200;
				} else {
					*is_obs_right-=2;
					*is_obs_left-=2;
					if (*is_obs_right < 0) *is_obs_right = 0;
					if (*is_obs_left < 0) *is_obs_left = 0;
					return 2 * this->old_theta;
				}

			}

		}
	}
	else if (*is_obs_left > *is_obs_right) {
		cout << "On left" << endl;
		if (right_empty) return -200;

		if (!left_empty && !right_empty) {
				double offset = 0;
				bool isAbnormal = false;

				double theta = 0;
				_abnormal_line(offset, &theta, &isAbnormal);
				if (isAbnormal) return 1.5 * theta;

		}

		int offset = -10;
		double center_x = center_y * center_y * this->rightLane->best.at<float>(0, 2) +
							center_y * this->rightLane->best.at<float>(0, 1) +
							this->rightLane->best.at<float>(0, 0) + offset;


		double dy = 150 - center_y;
		double dx = -center_x + 150 / 2;

		return 2 * this->_find_theta(dx, dy);
		
	}

	else if (*is_obs_right > *is_obs_left) {
		cout << "On right" << endl;
		if (left_empty) return 200;

		if (!left_empty && !right_empty) {
				double offset = 0;
				bool isAbnormal = false;
				double theta = 0;
				_abnormal_line(offset, &theta, &isAbnormal);
				if (isAbnormal) return 1.5 * theta;

		}

		double offset = 10;
		double center_x = center_y * center_y * this->leftLane->best.at<float>(0, 2) +
							center_y * this->leftLane->best.at<float>(0, 1) +
							this->leftLane->best.at<float>(0, 0) + offset;


		double dy = 150 - center_y;
		double dx = -center_x + 150 / 2;
		
		return 2 * this->_find_theta(dx, dy);
	}

//<-========================Find theta====================================->
	if (is_turn_left && !left_empty) {
		int offset = 20;
		double center_x = center_y * center_y * this->leftLane->best.at<float>(0, 2) +
							center_y * this->leftLane->best.at<float>(0, 1) +
							this->leftLane->best.at<float>(0, 0) + offset;


		double dy = 150 - center_y;
		double dx = -center_x + 150 / 2;

		return this->_find_theta(dx, dy);
	}

	if (is_turn_right && !right_empty) {
		int offset = -20;
		double center_x = center_y * center_y * this->rightLane->best.at<float>(0, 2) +
							center_y * this->rightLane->best.at<float>(0, 1) +
							this->rightLane->best.at<float>(0, 0) + offset;
		


		double dy = 150 - center_y;
		double dx = -center_x + 150 / 2;

		return this->_find_theta(dx, dy);
	}

	//cout << "Begin: " << endl;


	if (!right_empty && !left_empty) {


		bool isAbnormal = false;
		double theta = 0;
		_abnormal_line(0, &theta, &isAbnormal);
		if (isAbnormal) {
			return theta;
		} else {	
		
			add(this->rightLane->best,this->leftLane->best,this->center_poly);
			divide(this->center_poly, 2, this->center_poly);
		}
	} else if (right_empty && !left_empty) {
		double left_x2 = this->leftLane->best.at<float>(0, 2), left_x1 = this->leftLane->best.at<float>(0, 1), left_x0 = this->leftLane->best.at<float>(0, 0);
		cout << "Left: " << left_x2 << "x^2 + " << left_x1 << "x + " << left_x0 << endl;

		double top_x = top_y * top_y * left_x2 + top_y * left_x1 + left_x0;
		double bottom_x = bottom_y * bottom_y * left_x2 + bottom_y * left_x1 + left_x0;

		if (top_x - bottom_x < -1) {
			this->leftLane->release();
			//this->old_theta = 200;
			//return 200;
		} else if (top_x - bottom_x > 1) {
			//this->old_theta = -200;
			//return -200;
		}

		else return 3 * this->old_theta;
		//return 0;

		double theta = 2 * this->_find_theta(-top_x + bottom_x , 150);
		if (abs(theta) < 40) theta = theta * 3;
		return theta;

	} else if (!right_empty && left_empty) {
		double right_x2 = this->rightLane->best.at<float>(0, 2), right_x1 = this->rightLane->best.at<float>(0, 1), right_x0 = this->rightLane->best.at<float>(0,0);
		cout << "Right: " << right_x2 << "x^2 + " << right_x1 << "x + " << right_x0 << endl;

		double top_x = top_y * top_y * right_x2 + top_y * right_x1 + right_x0;
		double bottom_x = bottom_y * bottom_y * right_x2 + bottom_y * right_x1 + right_x0;

		if (top_x - bottom_x > 1) {
			this->rightLane->release();
			//this->old_theta = -200;
			//return -200;
		} else if (top_x - bottom_x < -1) {
			//this->old_theta = 200;
			//return 200;
		}
		else return 3 * this->old_theta;

		//return 0;
		
		double theta = 2 * this->_find_theta(-top_x + bottom_x, 150);
		if (abs(theta) < 40) theta = theta * 3;
		return theta;

	} 
	

	double center_x = center_y * center_y * this->center_poly.at<float>(0, 2) +
							center_y * this->center_poly.at<float>(0, 1) +
							this->center_poly.at<float>(0, 0);
		

	double bottom_x = bottom_y * bottom_y * this->center_poly.at<float>(0, 2) +
							bottom_y * this->center_poly.at<float>(0, 1) +
							this->center_poly.at<float>(0, 0);


	//cout << "x: " << center_x << " real_x: " << real_center_x << endl;

	double pi = acos(-1.0);
	double theta1, theta2;


	//double dx = (-center_x - 2 * center_x_2)/3 + 150 / 2;
	
	double dx1 = -center_x + 150 / 2;
	double dx2 = -bottom_x + 150 /2 ;
	double dy1 = 150 - center_y;
	double dy2 = 150 / 2;

	if (dx1 < 0) theta1 = -atan(-dx1 / dy1) * 180 / pi;
	else theta1 = atan(dx1 / dy1) * 180 / pi;

	if (dx2 < 0) theta2 = -atan(-dx2 / dy2) * 180 / pi;
	else theta2 = atan(dx2 / dy2) * 180 / pi;
	

	cout << "theta1: " << theta1 << " theta2: " << theta2 << endl;
	
	double theta = theta1;


	//if (abs(theta1) < 10) theta += theta2 ;
	
	theta += theta2;
	

	if (this->isTest) {
		
		LaneFinder::_draw_poly(polynomial, this->center_poly);


		imshow("Center", polynomial);
	}



	transformed.release();
	binary.release();
	this->center_poly.release();
	theta = 3* theta;
	this->old_theta = theta;
	return theta;
}

float LaneFinder::_find_theta(double dx, double dy) {
	double theta;
	double pi = acos(-1.0);
	if (dx < 0) theta = -atan(-dx / dy) * 180 / pi;
	else theta = atan(dx / dy) * 180 / pi;
	theta = 3 * theta;
	this->old_theta = theta;
	return theta;
}


void LaneFinder::_transform_perspective_ver1(const Mat& src, Mat& dst) {
	Mat temp;
	warpPerspective(src, temp, this->M, Size(this->transformed_w, this->transformed_h + 15));
	Rect roi(0, 0, 150, 150);
	dst = temp(roi);
}

void LaneFinder::_transform_perspective_ver2(const Mat& src, Mat& dst) {
	Mat temp;
	warpPerspective(src, temp, this->M, Size(this->transformed_w, this->transformed_h + 35));
	Rect roi(0, 0, 150, 150);
	dst = temp(roi);
}


void LaneFinder::_binary_image_after_ver1(const Mat& bgr_img, Mat& dest) {
	Mat hls_img, temp;

	cvtColor(bgr_img, hls_img, CV_BGR2HLS);


	vector<Mat> channels(3);

	split(hls_img, channels);

	Mat l_hls_img = channels[1];

	double max_l;

	minMaxLoc(l_hls_img, NULL, &max_l, NULL, NULL);

	l_hls_img = l_hls_img * 255 / max_l;
	GaussianBlur(l_hls_img, l_hls_img, Size(11,11), 0);
	threshold(l_hls_img, temp, 200, 255, THRESH_BINARY);

	dest = _remOutlier(temp);
}

void LaneFinder::_binary_image_after_ver2(const Mat& bgr_img, Mat& dest) {
	Mat gray, temp;

	cvtColor(bgr_img, gray, CV_BGR2GRAY);

	GaussianBlur(gray, gray, Size(11,11), 0);
	threshold(gray, temp, 210, 255, THRESH_BINARY);

	dest = _remOutlier(temp);

}

void LaneFinder::_binary_image_before_ver1(const Mat& src, Mat& dst) {
	Rect roi(0, 280, 640, 200);
	Mat temp = src(roi);
	Mat gray;
	cvtColor(temp, gray, CV_BGR2GRAY);
	
	GaussianBlur(gray, gray, Size(13, 13), 0);
	Mat img_thresh;
	threshold(gray, img_thresh, 200, 255, THRESH_BINARY);

	dst = this->_remOutlier(img_thresh);
}

cv::Mat LaneFinder::_remOutlier(const cv::Mat &gray) {

    int esize = 1;
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
        cv::Size( 2*esize + 1, 2*esize+1 ),
        cv::Point( esize, esize ) );
    cv::erode(gray, gray, element);
  
  std::vector< std::vector<cv::Point> > contours, polygons;
    std::vector< cv::Vec4i > hierarchy;
    cv::findContours(gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
    for (size_t i = 0; i < contours.size(); ++i) {
        std::vector<cv::Point> p;
        cv::approxPolyDP(cv::Mat(contours[i]), p, 2, true);
        polygons.push_back(p);
    }
    cv::Mat poly = cv::Mat::zeros(gray.size(), CV_8UC1);
    for (size_t i = 0; i < polygons.size(); ++i) {
        cv::Scalar color = cv::Scalar(255, 255, 255);
        cv::drawContours(poly, polygons, i, color, CV_FILLED);
    }
    return poly;
}

void LaneFinder::_draw_poly(Mat& img, const Mat& poly) {
	std::vector<Point> points;
	for (int y = 0; y < img.rows; y++) {

		double x = poly.at<float>(0, 2) * y * y + poly.at<float>(0, 1) * y + poly.at<float>(0, 0);
		circle(img, Point(int(x), y), 1, Scalar(255, 255, 255));
	} 

}


void LaneFinder::_abnormal_line(double offset, double *theta, bool *isAbnormal) {

		//If found left and right lane

		double left_x2 = this->leftLane->best.at<float>(0, 2), left_x1 = this->leftLane->best.at<float>(0, 1), left_x0 = this->leftLane->best.at<float>(0, 0);
		double right_x2 = this->rightLane->best.at<float>(0, 2), right_x1 = this->rightLane->best.at<float>(0, 1), right_x0 = this->rightLane->best.at<float>(0,0);

		cout << "Right: " << right_x2 << "x^2 + " << right_x1 << "x + " << right_x0 << endl;
		cout << "Left: " << left_x2 << "x^2 + " << left_x1 << "x + " << left_x0 << endl;
		
		Mat lrdiff = Mat::zeros(1, 3, CV_32FC1);	
		Mat diff = Mat::zeros(1, 3, CV_32FC1);			
		absdiff(this->rightLane->best, this->leftLane->best, lrdiff);

		cout <<"Diff: " << right_x2-left_x2 << "x^2 + " << right_x1-left_x1 << "x + " << right_x0-left_x0 << ", theta = " << (right_x1-left_x1)*(right_x1-left_x1) - 4*(right_x2-left_x2)*(right_x0-left_x0) <<  endl;

		cout << "Left px count: " << this->leftLane->px_count << ", Right px count: " << this->rightLane->px_count << endl;

		
		double theta_diff = (right_x1-left_x1)*(right_x1-left_x1) - 4*(right_x2-left_x2)*(right_x0-left_x0);
		bool isIntersect_high = false, isIntersect_low = false;
		if (theta_diff > 0) {
			double y1 = (-(right_x1-left_x1) + sqrt(theta_diff)) / (2 * (right_x2-left_x2));
			double y2 = (-(right_x1-left_x1) - sqrt(theta_diff)) / (2 * (right_x2-left_x2));
			cout << "y1: " << y1 << ", y2: " << y2 << endl;
			if ((y1 >= 30 && y1 <= 150) || (y2 >= 30 && y2 <= 150)) isIntersect_high = true; 
			//else if ((y1 >= 45 && y1 <= 150) || (y2 >= 45 && y2 <= 150)) isIntersect_low = true; 
		}

		if ((lrdiff.at<float>(0,2) < 0.01 && lrdiff.at<float>(0,1) < 0.1 && lrdiff.at<float>(0,0) < 10)) {
			*isAbnormal = true;
			cout << "Same: " << endl;

			double top_x;
			double bottom_x;
				
			if (this->rightLane->px_count < 200 && this->leftLane->px_count < 200) {
				*theta = this->old_theta;
				return;
			}
			
			if (this->rightLane->px_count >= this->leftLane->px_count) {
				top_x = top_y * top_y * right_x2 + top_y * right_x1 + right_x0;
				bottom_x = bottom_y * bottom_y * right_x2 + bottom_y * right_x1 + right_x0;
			} else {
				top_x = top_y * top_y * left_x2 + top_y * left_x1 + left_x0;
				bottom_x = bottom_y * bottom_y * left_x2 + bottom_y * left_x1 + left_x0;
			}

			if (top_x - bottom_x < -1) {
				this->leftLane->release();
				//return 200;
			}
			else if (top_x - bottom_x > 1) {
				this->rightLane->release();
				//return -200;
			}
			
			else *theta = this->old_theta;

			//return 0;
	
			*theta =  2 * this->_find_theta(-top_x + bottom_x + offset, 150);
			if (abs(*theta) < 40) *theta = *theta * 3;


		} else if (isIntersect_low || isIntersect_high) {
			*isAbnormal = true;
			cout << "Intersect: " << endl;

			double top_x_left;
			double bottom_x_left;

			double top_x_right;
			double bottom_x_right;

			double top_x, bottom_x;		

			top_x_right = top_y * top_y * right_x2 + top_y * right_x1 + right_x0;
			bottom_x_right = bottom_y * bottom_y * right_x2 + bottom_y * right_x1 + right_x0;
			
			top_x_left = top_y * top_y * left_x2 + top_y * left_x1 + left_x0;
			bottom_x_left = bottom_y * bottom_y * left_x2 + bottom_y * left_x1 + left_x0;

			double diff_right = abs(top_x_right-bottom_x_right);
			double diff_left = abs(top_x_left - bottom_x_left);

			if ((diff_right < 20 && diff_left < 20)) {
				if (this->rightLane->px_count >= this->leftLane->px_count) {
					top_x = top_x_right;
					bottom_x = bottom_x_right;
				} else {
					top_x = top_x_left;
					bottom_x = bottom_x_left;				
				}
			} else if (diff_right > diff_left) {
				top_x = top_x_right;
				bottom_x = bottom_x_right;
			} else {
				top_x = top_x_left;
				bottom_x = bottom_x_left;
			}

			if (top_x - bottom_x < -1) {
				this->leftLane->release();
				//return 200;
			}
			else if (top_x - bottom_x > 1) {
				this->rightLane->release();
				//return -200;
			}
			else *theta = this->old_theta;

			//return 0;

			if (isIntersect_high) *theta = this->_find_theta(-top_x + bottom_x + offset, 150);
			else *theta = 1.5 * this->_find_theta(-top_x + bottom_x + offset, 150);

		} 

		else *isAbnormal = false;
}


