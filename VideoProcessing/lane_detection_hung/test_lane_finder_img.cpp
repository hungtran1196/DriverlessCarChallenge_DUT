#include "laneFinder.h"

int main(int argc, char** argv) {
    if( argc != 2)
    {
     	cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }

    LaneFinder laneFinder(true);

    Mat image;
    image = imread(argv[1], CV_LOAD_IMAGE_COLOR);   // Read the file

    if(! image.data )                              // Check for invalid input 
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    cout << "Begin" << endl;

    Mat temp;

    auto bt = chrono::high_resolution_clock::now();

    double theta = laneFinder.findTheta(temp);
    printf("Run in %.2fms \n", chrono::duration<double, milli> (chrono::high_resolution_clock::now() - bt).count());

    cout<<theta<<endl;

    waitKey(0);

    return 0;


    // std::vector<int> v;
    // v.push_back(2); v.push_back(2);

    // std::vector<int> u;
    // u.push_back(1); u.push_back(4);

    // std::vector<int> rs;

    // //compare(v, u, rs, CMP_GT);

    // pow(v, 2, rs);

    // cout << rs[0];
}
