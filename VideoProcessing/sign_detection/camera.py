#!/usr/bin/python
import cv2
import numpy as np
from primesense import openni2
from primesense import _openni2 as c_api
openni2.initialize("/home/ubuntu/Downloads/2-Linux/OpenNI-Linux-Arm-2.3/Redist")
dev = openni2.Device.open_any()
color_stream = dev.create_color_stream()
color_stream.set_video_mode(c_api.OniVideoMode(pixelFormat = c_api.OniPixelFormat.ONI_PIXEL_FORMAT_RGB888, resolutionX = 640, resolutionY = 480, fps = 30))
color_stream.start()
while True:
	frame = color_stream.read_frame()
	frame_data = frame.get_buffer_as_uint8()
	img = np.frombuffer(frame_data, dtype=np.uint8)
	img.shape = (480,640,3)

	cv2.imshow("image", img)
	cv2.waitKey(34)
openni2.unload()

