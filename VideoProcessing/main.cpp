
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <unistd.h>
#include <chrono>
#include <stdlib.h>
#include "Hal.h"
#include "LCDI2C.h"
#include "api_i2c_pwm.h"
#include "laneFinder.h"
#include "square_detector.h"

#include <thread>
#include <mutex>

using namespace cv;
using namespace EmbeddedFramework;
#define SAMPLE_READ_WAIT_TIMEOUT 2000 //2000ms
#undef debug
#define debug false
#define SW1_PIN	160
#define SW2_PIN	161
#define SW3_PIN	163
#define SW4_PIN	164
#define SENSOR	165
#define LED		166

mutex mAnh;
mutex mSquares;
vector<vector<Point>> squares;
Mat frame;
SquareDetector *square_detector = new SquareDetector(false);
Mat hinhanh(Mat img = Mat()){
	lock_guard<mutex> lock(mAnh);
	if (img.empty()){
		return frame;
	}
	else{
//		imshow("color", img);
//		waitKey(1);
		frame = img.clone();
		return Mat();
	}
}


cv::Mat remOutlier(const cv::Mat &gray) {
    int esize = 1;
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
    cv::Size( 2*esize + 1, 2*esize+1 ),
    cv::Point( esize, esize ) );
    cv::erode(gray, gray, element);
    std::vector< std::vector<cv::Point> > contours, polygons;
    std::vector< cv::Vec4i > hierarchy;
    cv::findContours(gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
    for (size_t i = 0; i < contours.size(); ++i) {
        std::vector<cv::Point> p;
        cv::approxPolyDP(cv::Mat(contours[i]), p, 2, true);
        polygons.push_back(p);
    }
    cv::Mat poly = cv::Mat::zeros(gray.size(), CV_8UC3);
    for (size_t i = 0; i < polygons.size(); ++i) {
        cv::Scalar color = cv::Scalar(255, 255, 255);
        cv::drawContours(poly, polygons, i, color, CV_FILLED);
    }
    return poly;
}


void set_squares(vector<vector<Point>> s) {
	lock_guard<mutex> lock(mSquares);
	squares = s;
}

vector<vector<Point>> get_squares() {
	lock_guard<mutex> lock(mSquares);
	return squares;
}

void tim_vat_can() {
	Mat img;
	while (1) {
		img = hinhanh();
		if (!img.empty()) {
			vector<vector<Point>> s;
			square_detector->findSquares(img, s);
			set_squares(s);
		}
	}
}

///////// utilitie functions  ///////////////////////////

int main( int argc, char* argv[] ) {
	////// init videostream ///
	GPIO *gpio = new GPIO();
	I2C *i2c_device = new I2C();
	LCDI2C *lcd = new LCDI2C();
    	int sw1_stat = 1;
	int sw2_stat = 1;
	int sw3_stat = 1;
	int sw4_stat = 1;
	int sensor = 1;
	
	// Setup input
	gpio->gpioExport(SW1_PIN);
	gpio->gpioExport(SW2_PIN);
	gpio->gpioExport(SW3_PIN);
	gpio->gpioExport(SW4_PIN);
	gpio->gpioExport(SENSOR);
	gpio->gpioExport(LED);
	gpio->gpioSetDirection(SW1_PIN, INPUT);
	gpio->gpioSetDirection(SW2_PIN, INPUT);
	gpio->gpioSetDirection(SW3_PIN, INPUT);
	gpio->gpioSetDirection(SW4_PIN, INPUT);
	gpio->gpioSetDirection(SENSOR, INPUT);
	gpio->gpioSetDirection(LED, OUTPUT);
	i2c_device->m_i2c_bus = 2;

	
	if (!i2c_device->HALOpen()) {
		printf("Cannot open I2C peripheral\n");
		exit(-1);
	} else printf("I2C peripheral is opened\n");
	
	unsigned char data;
	if (!i2c_device->HALRead(0x38, 0xFF, 0, &data, "")) {
		printf("LCD is not found!\n");
		exit(-1);
	} else printf ("LCD is connected\n");
	usleep(10000);
	lcd->LCDInit(i2c_device, 0x38, 20, 4);
	lcd->LCDBacklightOn();
	lcd->LCDCursorOn();
	
	lcd->LCDSetCursor(3,1);
	lcd->LCDPrintStr("DRIVERLESS CAR");
	lcd->LCDSetCursor(5,2);
	lcd->LCDPrintStr("2017-2018");
	int dir = 0, throttle_val = 0;
	double theta = 0;
	int current_state = 0;
	char key = 0;

	    //=========== Init  =======================================================
	    ////////  Init PCA9685 driver   ///////////////////////////////////////////
	PCA9685 *pca9685 = new PCA9685() ;
	api_pwm_pca9685_init( pca9685 );
	if (pca9685->error >= 0)api_set_FORWARD_control( pca9685,throttle_val);
	    /// Init MSAC vanishing point library

	int set_throttle_val = 0;
    	throttle_val = 0;
   	theta = 0;
    	if(argc > 1 ) set_throttle_val = atoi(argv[1]);
    	fprintf(stderr, "Initial throttle: %d\n", set_throttle_val);

	bool running = false, started = false, stopped = false;
	
	cout << argv[2]<<endl;

	Mat colorImg;
	LaneFinder laneFinder(1);
	laneFinder.init();
	VideoCapture cap(argv[2]);

	std::thread third(tim_vat_can);
	int is_obs_left = 0; int is_obs_right = 0;
    	while ( true )
    	{	
		Point center_point(0,0);
		key = getkey();
       		unsigned int bt_status = 0;
		unsigned int sensor_status = 0;
		gpio->gpioGetValue(SW4_PIN, &bt_status);
		gpio->gpioGetValue(SENSOR, &sensor_status);
		//std::cout<<sensor_status<<std::endl;
		if (!bt_status) {
			if (bt_status != sw4_stat) {
				running = !running;
				sw4_stat = bt_status;
				throttle_val = set_throttle_val;
			}
		} else sw4_stat = bt_status;
	
        	if( key == 's') {
			running = !running;
			throttle_val = set_throttle_val;
			
		}
       		if( key == 'f') {
			fprintf(stderr, "End process.\n");
        		theta = 0;
        		throttle_val = 0;
	    		api_set_FORWARD_control( pca9685,throttle_val);
        		break;
		}
		if( !running ) {
			lcd->LCDClear();
			lcd->LCDSetCursor(3,1);
			lcd->LCDPrintStr("PAUSE");
			continue;
		}
		if( running ){
			lcd->LCDClear();
			lcd->LCDSetCursor(3,1);
			lcd->LCDPrintStr("RUNNING");
		
		if (!sensor_status) {
			if (sensor_status != sensor) {
				running = !running;
				sensor = sensor_status;
				throttle_val = 0;
			}
		} else sensor = sensor_status;
		
		if (pca9685->error < 0) {
                	cout<< endl<< "Error: PWM driver"<< endl<< flush;
                	break;
           	 }
		
		if (!started) {
    			fprintf(stderr, "ON\n");
			started = true; stopped = false;
			throttle_val = set_throttle_val;
                	api_set_FORWARD_control( pca9685,throttle_val);
		}


		cap >> colorImg;




		Mat temp;

		resize(colorImg, temp, Size(640, 480));	
		hinhanh(temp);	
		//flip(temp, temp, 1);
		cout << colorImg.rows << " "<<colorImg.cols<<" "<<temp.rows<<" "<<temp.cols<<endl;
		auto bt = chrono::high_resolution_clock::now();
 		
		vector<vector<Point>> s = get_squares();
		cout << "Square: " << s.size() << endl;
		if(s.size()) {
			Mat temp_square = colorImg.clone();
			square_detector->drawSquares(temp_square, s);
			imshow("Square", temp_square);
		}

		
 		double angDiff = laneFinder.findTheta(temp, false, false ,&is_obs_left, &is_obs_right, s);
		if (is_obs_left) is_obs_left--;
		if (is_obs_right) is_obs_right--;
		
		printf("Lane Run in %.2fms \n", chrono::duration<double, milli> (chrono::high_resolution_clock::now() - bt).count());
		// theta = (0.00);
		theta = angDiff;
		cout << "angle: " << angDiff << endl;
		api_set_STEERING_control(pca9685,theta);
            	int pwm2 =  api_set_FORWARD_control( pca9685,throttle_val);

		
			
		waitKey(0);

		}
	}

}


