# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/ubuntu/DriverlessCarChallenge_DUT/VideoProcessing/HAL/Hal.cpp" "/home/ubuntu/DriverlessCarChallenge_DUT/VideoProcessing/HAL/CMakeFiles/HAL.dir/Hal.cpp.o"
  "/home/ubuntu/DriverlessCarChallenge_DUT/VideoProcessing/HAL/LCDI2C.cpp" "/home/ubuntu/DriverlessCarChallenge_DUT/VideoProcessing/HAL/CMakeFiles/HAL.dir/LCDI2C.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/ubuntu/Downloads/2-Linux/OpenNI-Linux-Arm-2.3/Include"
  "/usr/local/include"
  "/usr/local/include/opencv"
  "peripheral_driver/i2c"
  "peripheral_driver/uart"
  "lane_detection_hung"
  "HAL"
  "obstacle_detection"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
