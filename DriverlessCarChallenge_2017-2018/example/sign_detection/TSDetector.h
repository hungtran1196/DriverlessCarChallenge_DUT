#include <Python.h>
#include <iostream>
#include <numpy/ndarrayobject.h>
#include "pyboostcvconverter.hpp"
#include <chrono>
using namespace pbcvt;
using namespace cv;
using namespace std;

class TSDetector {
	PyObject *pName, *pModule, *pDict, *init_predict, *close_predict, *predict;
	PyObject *pArgs, *pValue;

	public:
		TSDetector(string python_path=".");
		~TSDetector();
		int detect(const Mat& m);
};
