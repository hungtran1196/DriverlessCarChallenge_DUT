# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/ubuntu/DriverlessCarChallenge_DUT/DriverlessCarChallenge_2017-2018/example/radon/radon.cpp" "/home/ubuntu/DriverlessCarChallenge_DUT/DriverlessCarChallenge_2017-2018/example/radon/CMakeFiles/radon.dir/radon.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/ubuntu/DriverlessCarChallenge_DUT/DriverlessCarChallenge_2017-2018/example/lane_detection/CMakeFiles/vanishing-point.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/ubuntu/Downloads/2-Linux/OpenNI-Linux-Arm-2.3/Include"
  "/usr/local/include"
  "/usr/local/include/opencv"
  "/usr/include/python3.4m"
  "/usr/include/arm-linux-gnueabihf/python3.4m"
  "stereo_vision"
  "lane_detection"
  "lane_detection_hung"
  "lane_detection/msac"
  "peripheral_driver/i2c"
  "peripheral_driver/uart"
  "HAL"
  "include"
  "radon"
  "extract_info"
  "openni2"
  "sign_detection"
  "obstacle_detection"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
