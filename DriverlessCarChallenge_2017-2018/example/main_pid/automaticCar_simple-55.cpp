#include "api_kinect_cv.h"
#include <iostream>
#include <opencv2/imgproc.hpp>
#include "openni2.h"
#include "../openni2/Singleton.h"
#include <unistd.h>
#include <chrono>
#include <future>
#include "extractInfo.h"
#include <stdlib.h>
#include "Hal.h"
#include "LCDI2C.h"
#include "api_i2c_pwm.h"
#include "laneFinder.h"
#include "TSDetector.h"
#include <thread>
#include <mutex>

using namespace std;
using namespace openni;
using namespace framework;
using namespace EmbeddedFramework;
#define SAMPLE_READ_WAIT_TIMEOUT 2000 //2000ms
#undef debug
#define debug false
#define SW1_PIN	160
#define SW2_PIN	161
#define SW3_PIN	163
#define SW4_PIN	164
#define SENSOR	165
#define LED	166
///////// utilitie functions  ///////////////////////////




//Init traffic-sign detector
bool isStop = false;
Mat frame;

TSDetector *ts_detector;

int re_trai_id = 3, re_phai_id = 2;
int count_re_trai = 0, count_re_phai = 0;
int obstacle_value = 1;
bool isObstacle = false;

mutex mAnh;
mutex mBien;
mutex can1, can2;

Mat hinhanh(Mat img = Mat()){
	lock_guard<mutex> lock(mAnh);
	if (img.empty()){
		return frame;
	}
	else{
//		imshow("color", img);
//		waitKey(1);
		frame = img.clone();
		return Mat();
	}
}

int get_set_obstacle_value(int value = -1) {
	lock_guard<mutex> lock(can1);
	if (value == -1) {
		return obstacle_value;
	} else {
		obstacle_value = value;
		return -1;
	}
}

bool get_set_is_obstacle(int value = -1) {
	lock_guard<mutex> lock(can2);
	if (value == -1) {
		return isObstacle;	
	} else {
		isObstacle = value;
	}
}

int bienbao(int value, int type) {
	lock_guard<mutex> lock(mBien);
	if (type == re_trai_id) {
		count_re_trai += value;
		return count_re_trai;
	}

	if (type == re_phai_id) {
		count_re_phai += value;
		return count_re_phai;
	}
}

void xu_ly_vat_can() {
	int prev_value = get_set_obstacle_value();
	int time_out = 5000;
	while(1) {
		int current_value = get_set_obstacle_value();
		if (current_value == 0 && prev_value == 1) {
			get_set_is_obstacle(1);
			this_thread::sleep_for(chrono::milliseconds(time_out));
			get_set_is_obstacle(0);	
		}
		prev_value = current_value;
		
	}
}

void increase_sign_count() {
	PyThreadState *_save;
	_save = PyEval_SaveThread();

	while(1) {
		Mat temp = hinhanh();
		if (!temp.empty()) {

			PyGILState_STATE gstate;
			gstate = PyGILState_Ensure();
			//auto bt = chrono::high_resolution_clock::now();


			int sign = ts_detector->detect(temp);
			//cout << sign << endl;
			//printf("Ts Run in %.2fms \n", chrono::duration<double, milli> (chrono::high_resolution_clock::now() - bt).count());

			if (sign == re_trai_id) bienbao(1, re_trai_id);
			else if (sign == re_phai_id) bienbao(1, re_phai_id);

			PyGILState_Release(gstate);
		}	
		if (isStop) break;
	}

	PyEval_RestoreThread(_save);


}

void decrease_sign_count() {
	int timeout_base = 200;
	while(1) {
		int count_re_trai = bienbao(0, re_trai_id);
		int count_re_phai = bienbao(0, re_phai_id);

		if (count_re_phai || count_re_trai) {
			int max_count = max(count_re_trai, count_re_phai);
			int time_out = int(timeout_base * 1.0 / ((max_count / 10 + 1) * (max_count / 10 + 1) * (max_count/10 + 1)));
			cout << time_out << endl;

			this_thread::sleep_for(chrono::milliseconds(time_out));
			if (count_re_phai) bienbao(-1, re_phai_id);
			if (count_re_trai) bienbao(-1, re_trai_id);
		}

		if (isStop) break;
	}
}


int main( int argc, char* argv[] ) {

	ts_detector = new TSDetector("./sign_detection/");

	////// init videostream ///
	GPIO *gpio = new GPIO();
	I2C *i2c_device = new I2C();
	LCDI2C *lcd = new LCDI2C();
    	int sw1_stat = 1;
	int sw2_stat = 1;
	int sw3_stat = 1;
	int sw4_stat = 1;
	int sensor = 1;
	
	// Setup input
	gpio->gpioExport(SW1_PIN);
	gpio->gpioExport(SW2_PIN);
	gpio->gpioExport(SW3_PIN);
	gpio->gpioExport(SW4_PIN);
	gpio->gpioExport(SENSOR);
	gpio->gpioExport(LED);
	gpio->gpioSetDirection(SW1_PIN, INPUT);
	gpio->gpioSetDirection(SW2_PIN, INPUT);
	gpio->gpioSetDirection(SW3_PIN, INPUT);
	gpio->gpioSetDirection(SW4_PIN, INPUT);
	gpio->gpioSetDirection(SENSOR, INPUT);
	gpio->gpioSetDirection(LED, OUTPUT);
	i2c_device->m_i2c_bus = 2;
	
	if (!i2c_device->HALOpen()) {
		printf("Cannot open I2C peripheral\n");
		exit(-1);
	} else printf("I2C peripheral is opened\n");
	
	unsigned char data;
	if (!i2c_device->HALRead(0x38, 0xFF, 0, &data, "")) {
		printf("LCD is not found!\n");
		exit(-1);
	} else printf ("LCD is connected\n");
	usleep(10000);
	lcd->LCDInit(i2c_device, 0x38, 20, 4);
	lcd->LCDBacklightOn();
	lcd->LCDCursorOn();
	
	lcd->LCDSetCursor(3,1);
	lcd->LCDPrintStr("DRIVERLESS CAR");
	lcd->LCDSetCursor(5,2);
	lcd->LCDPrintStr("2017-2018");
	int dir = 0, throttle_val = 0;
	double theta = 0;
	int current_state = 0;
	char key = 0;

	    //=========== Init  =======================================================
	    ////////  Init PCA9685 driver   ///////////////////////////////////////////
	PCA9685 *pca9685 = new PCA9685() ;
	api_pwm_pca9685_init( pca9685 );
	if (pca9685->error >= 0)api_set_FORWARD_control( pca9685,throttle_val);

	int set_throttle_val = 0;
    throttle_val = 0;
   	theta = 0;
    if(argc > 1 ) set_throttle_val = atoi(argv[1]);
    fprintf(stderr, "Initial throttle: %d\n", set_throttle_val);
    int frame_width = VIDEO_FRAME_WIDTH;
    int frame_height = VIDEO_FRAME_HEIGHT;

	bool running = false, started = false, stopped = false;
	cout << "Stating main"<<endl;
	OpenNI2::Instance() -> init();
	
	ushort l_th = 600, h_th = 2000;
	Mat depthImg, colorImg, grayImage, disparity;
    
	int isTest = atoi(argv[2]);
	int isSave = atoi(argv[3]);
	isTest = isTest == 1;
	isSave = isSave == 1;
	cout << isTest << " " << isSave << endl;
	LaneFinder laneFinder(isTest);
	laneFinder.init();
    	VideoWriter color_videoWriter;
	string color_filename = "color.avi";

	int codec = CV_FOURCC('M','J','P', 'G');
	Size output_size(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT);
        color_videoWriter.open(color_filename, codec, 8, output_size*2, true);

//==================================== Init thread ==================================//
	std::thread first(increase_sign_count);
	std::thread second(decrease_sign_count);
	std::thread third(xu_ly_vat_can);

	bool is_re_trai = false, is_re_phai = false;
	int count_boundary = 2;
    	while ( true ) {	
		if (isStop) {
			throttle_val = 0;
			api_set_FORWARD_control(pca9685, throttle_val);
			break;
		}
		Point center_point(0,0);
		key = getkey();
       		unsigned int bt_status = 0;
		unsigned int stop_status = 0;
		unsigned int sensor_status = 0;
		gpio->gpioGetValue(SW4_PIN, &bt_status);
		gpio->gpioGetValue(SW2_PIN, &stop_status);
		gpio->gpioGetValue(SENSOR, &sensor_status);
		get_set_obstacle_value(sensor_status);
		if (!bt_status) {
			if (bt_status != sw4_stat) {
				running = !running;
				sw4_stat = bt_status;
				throttle_val = set_throttle_val;
			}
		} else sw4_stat = bt_status;
	
		if (!stop_status) {
			if (stop_status != sw2_stat) {
				isStop = true;
				sw1_stat = stop_status;
				throttle_val = set_throttle_val;
			}
		} else sw2_stat = stop_status;

        	if( key == 's') {
			running = !running;
			throttle_val = set_throttle_val;
			
		}
       		if( key == 'f') {
			fprintf(stderr, "End process.\n");
        		theta = 0;
        		throttle_val = 0;
	    		api_set_FORWARD_control( pca9685,throttle_val);
        		break;
		}
		if( !running ) {
			lcd->LCDClear();
			lcd->LCDSetCursor(3,1);
			lcd->LCDPrintStr("PAUSE");
        		throttle_val = 0;
	    		api_set_FORWARD_control( pca9685,throttle_val);
			continue;
		}
		if( running ){
			lcd->LCDClear();
			lcd->LCDSetCursor(3,1);
			lcd->LCDPrintStr("RUNNING");
			/*
			if (!sensor_status) {
				if (sensor_status != sensor) {
					running = !running;
					sensor = sensor_status;
					throttle_val = 0;
				}
			} else sensor = sensor_status;*/
		
			if (pca9685->error < 0) {
		        	cout<< endl<< "Error: PWM driver"<< endl<< flush;
		        	break;
		   	 }
		
			if (!started) {
	    			fprintf(stderr, "ON\n");
				started = true; stopped = false;
				throttle_val = set_throttle_val;
		        	api_set_FORWARD_control( pca9685,throttle_val);
			}


			OpenNI2::Instance()->getData(colorImg, depthImg, grayImage, disparity);
			//imshow("color", depthImg);
			hinhanh(colorImg);
			//cout << colorImg.cols << " " << colorImg.rows << VIDEO_FRAME_WIDTH <<" " << VIDEO_FRAME_HEIGHT << endl;
			if (isSave) color_videoWriter << colorImg;

			int count_trai = bienbao(0, re_phai_id);
			int count_phai = bienbao(0, re_trai_id);
			bool isOstacle = get_set_is_obstacle();
				
			if (count_trai > count_phai && count_trai > count_boundary) is_re_trai = true;
			else is_re_trai = false;
			
			if (count_phai > count_trai && count_phai > count_boundary) is_re_phai = true;
			else is_re_phai = false;
			
			cout << "Vat can: " << isOstacle << endl;

			cout << "Count_trai: " << count_trai << ", Count_phai: " << count_phai << ", Is re trai: " << is_re_trai << ", Is re phai: " << is_re_phai << endl;			

			auto bt = chrono::high_resolution_clock::now();
 		    	double angDiff = laneFinder.findTheta(colorImg, is_re_trai, is_re_phai, false);
			printf("Lane Run in %.2fms \n", chrono::duration<double, milli> (chrono::high_resolution_clock::now() - bt).count());
		    	theta = angDiff;
			std::cout<<"angdiff"<<angDiff<<std::endl;
			// theta = (0.00);
			api_set_STEERING_control(pca9685,theta);

			//f (abs(theta) >= 30 && abs(theta) <= 60) throttle_val = 45;
			//else throttle_val = set_throttle_val;
		    	int pwm2 =  api_set_FORWARD_control( pca9685,throttle_val);

		
			
			if (waitKey(10)=='q') break;;
			if (isStop) break;

		}
	}
	if (isSave) {
        	color_videoWriter.release();
	}
	lcd->LCDClear();
	
	delete ts_detector;
	Py_Finalize();
   	return 0;
}


