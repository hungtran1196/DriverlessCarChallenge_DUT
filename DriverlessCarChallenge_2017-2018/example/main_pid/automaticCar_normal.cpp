#include "api_kinect_cv.h"
#include <iostream>
#include <opencv2/imgproc.hpp>
#include "openni2.h"
#include "../openni2/Singleton.h"
#include <unistd.h>
#include <chrono>
#include "extractInfo.h"
#include <stdlib.h>
#include "Hal.h"
#include "LCDI2C.h"
#include "api_i2c_pwm.h"
#include "laneFinder.h"
using namespace openni;
using namespace framework;
using namespace EmbeddedFramework;
#define SAMPLE_READ_WAIT_TIMEOUT 2000 //2000ms
#undef debug
#define debug false
#define SW1_PIN	160
#define SW2_PIN	161
#define SW3_PIN	163
#define SW4_PIN	164
#define SENSOR	165
#define LED		166
///////// utilitie functions  ///////////////////////////

int main( int argc, char* argv[] ) {
	////// init videostream ///
	GPIO *gpio = new GPIO();
	I2C *i2c_device = new I2C();
	LCDI2C *lcd = new LCDI2C();
    	int sw1_stat = 1;
	int sw2_stat = 1;
	int sw3_stat = 1;
	int sw4_stat = 1;
	int sensor = 1;
	
	// Setup input
	gpio->gpioExport(SW1_PIN);
	gpio->gpioExport(SW2_PIN);
	gpio->gpioExport(SW3_PIN);
	gpio->gpioExport(SW4_PIN);
	gpio->gpioExport(SENSOR);
	gpio->gpioExport(LED);
	gpio->gpioSetDirection(SW1_PIN, INPUT);
	gpio->gpioSetDirection(SW2_PIN, INPUT);
	gpio->gpioSetDirection(SW3_PIN, INPUT);
	gpio->gpioSetDirection(SW4_PIN, INPUT);
	gpio->gpioSetDirection(SENSOR, INPUT);
	gpio->gpioSetDirection(LED, OUTPUT);
	i2c_device->m_i2c_bus = 2;
	
	if (!i2c_device->HALOpen()) {
		printf("Cannot open I2C peripheral\n");
		exit(-1);
	} else printf("I2C peripheral is opened\n");
	
	unsigned char data;
	if (!i2c_device->HALRead(0x38, 0xFF, 0, &data, "")) {
		printf("LCD is not found!\n");
		exit(-1);
	} else printf ("LCD is connected\n");
	usleep(10000);
	lcd->LCDInit(i2c_device, 0x38, 20, 4);
	lcd->LCDBacklightOn();
	lcd->LCDCursorOn();
	
	lcd->LCDSetCursor(3,1);
	lcd->LCDPrintStr("DRIVERLESS CAR");
	lcd->LCDSetCursor(5,2);
	lcd->LCDPrintStr("2017-2018");
	int dir = 0, throttle_val = 0;
	double theta = 0;
	int current_state = 0;
	char key = 0;

	    //=========== Init  =======================================================
	    ////////  Init PCA9685 driver   ///////////////////////////////////////////
	PCA9685 *pca9685 = new PCA9685() ;
	api_pwm_pca9685_init( pca9685 );
	if (pca9685->error >= 0)api_set_FORWARD_control( pca9685,throttle_val);

	int set_throttle_val = 0;
    throttle_val = 0;
   	theta = 0;
    if(argc == 2 ) set_throttle_val = atoi(argv[1]);
    fprintf(stderr, "Initial throttle: %d\n", set_throttle_val);
    int frame_width = VIDEO_FRAME_WIDTH;
    int frame_height = VIDEO_FRAME_HEIGHT;

	bool running = false, started = false, stopped = false;

	OpenNI2::Instance() -> init();
	// signsRecognizer recognizer = signsRecognizer("/home/ubuntu/data/new_templates/templates.txt");
	ushort l_th = 600, h_th = 2000;
	Mat depthImg, colorImg, grayImage, disparity;
    LaneFinder laneFinder;
    while ( true ) {	
		key = getkey();
       	unsigned int bt_status = 0;
		unsigned int sensor_status = 0;
		gpio->gpioGetValue(SW4_PIN, &bt_status);
		gpio->gpioGetValue(SENSOR, &sensor_status);
		//std::cout<<sensor_status<<std::endl;
		if (!bt_status) {
			if (bt_status != sw4_stat) {
				running = !running;
				sw4_stat = bt_status;
				throttle_val = set_throttle_val;
			}
		} else sw4_stat = bt_status;
	
        if( key == 's') {
			running = !running;
			throttle_val = set_throttle_val;		
		}
       	if( key == 'f') {
			fprintf(stderr, "End process.\n");
        	theta = 0;
        	throttle_val = 0;
	    	api_set_FORWARD_control( pca9685,throttle_val);
        	break;
		}
		if( !running ) {
    		lcd->LCDClear();
    		lcd->LCDSetCursor(3,1);
    		lcd->LCDPrintStr("PAUSE");
    		continue;
		}
		if( running ){
    		lcd->LCDClear();
    		lcd->LCDSetCursor(3,1);
    		lcd->LCDPrintStr("RUNNING");
		
    		if (!sensor_status) {
    			if (sensor_status != sensor) {
    				running = !running;
    				sensor = sensor_status;
    				throttle_val = 0;
    			}
    		} else sensor = sensor_status;
    			if (pca9685->error < 0) {
                    cout<< endl<< "Error: PWM driver"<< endl<< flush;
                    break;
               	}
    			if (!started) {
        			fprintf(stderr, "ON\n");
    			    started = true; stopped = false;
    				throttle_val = set_throttle_val;
                    api_set_FORWARD_control( pca9685,throttle_val);
    			}
            	auto st = chrono::high_resolution_clock::now();
    			OpenNI2::Instance()->getData(colorImg, depthImg, grayImage, disparity);
    			Mat colorTemp = colorImg.clone();
    			auto bt = chrono::high_resolution_clock::now();
    		
                double theta = laneFinder.findTheta(colorTemp);
    			//if(-20<angDiff&&angDiff<20)angDiff=0;
                // theta = (angDiff*2);
    			std::cout<<"angdiff"<<angDiff<<std::endl;
    		       // theta = (0.00);
    		    api_set_STEERING_control(pca9685,theta);
                int pwm2 =  api_set_FORWARD_control( pca9685,throttle_val);
    			auto et = chrono::high_resolution_clock::now();
    		
    			vector<int> signLabels;
    			vector<string> names;
    			// recognizer.labeling(boxes, labels, colorImg, signLabels, names);
    			bt = chrono::high_resolution_clock::now();
    		
    			
    			char ch = waitKey(10);
    			if (ch == 'q')
    				break;
    			//////// End Detect traffic signs //////////////
	   }
	}
    return 0;
}


