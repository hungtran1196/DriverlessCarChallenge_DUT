#include "stdio.h"
#include <math.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"

#include <vector>
#include <iostream>
#include <iomanip>

#include <chrono>


using namespace std;
using namespace cv;


class Lane {
	public:
		bool detected = false;
		std::vector<Mat> fits;
		Mat best;
		Mat diff;
		int px_count = 0;
		bool isLeft = true;
		int margin = 20;
		int minpix = 10;
		int img_h, img_w;
		int num_not_found = 0;
		void find(const Mat& img);
		bool isEmpty();
		void release();
		Lane(bool isLeft, int h, int w);
		~Lane();
	private:
		void _add_fit(const Mat& currentFit, const std::vector<int> &lane_inds);
		void _sliding_window_polyfit(const Mat& img, Mat& fit, std::vector<int> &lane_inds);
		void _slidding_window_use_prev_fit(const Mat& img, const Mat& fit_prev, Mat& currentFit, std::vector<int> &lane_inds);
		void _polyfit(const Mat& src_x, const Mat& src_y, Mat& dst, int order);
		void _find_next_land_inds_using_prev_fit(const std::vector<int> &nonzerox, 
				const std::vector<int> &nonzeroy, const Mat &prevfit, 
				std::vector<int> &lane_inds);
		void _nonzero_idx_to_vector(const std::vector<Point> idx, std::vector<int> *x, std::vector<int> *y);
		void _nonzero_idx_to_mat(const std::vector<Point> &idx, Mat *x, Mat *y);
		void _cal_best();
		std::vector<float> _sub_vector(const std::vector<int> &main, const std::vector<int> &idx);
		int _vector_mean(const std::vector<float> &v);

};

