#include "lane.h"

Lane::Lane(bool isLeft, int h, int w) {
	this->diff = Mat::zeros(1, 3, CV_32FC1);
	this->isLeft = isLeft;
	this->img_h = h;
	this->img_w = w;
}

Lane::~Lane() {
	for (int i = 0; i < this->fits.size(); i++) {
		this->fits[i].release();
	}
	this->fits.clear();
	this->best.release();
}

void Lane::find(const Mat& img) {
	Mat temp;
	std::vector<int> lane_inds;
	//

	if (this->detected) {
		this->_slidding_window_use_prev_fit(img, this->best, temp, lane_inds);
	} else {
		this->_sliding_window_polyfit(img, temp, lane_inds);
	}


	this->_add_fit(temp, lane_inds);

}

void Lane::_add_fit(const Mat& currentFit, const std::vector<int> &lane_inds) {
	//cout << "Lane inds: " << lane_inds.size() << endl;
	//cout<<"Is best empy: "<<this->best.empty()<<endl;
	//cout << "Current fit empty: " << currentFit.empty() << endl;
	bool isCurrentFitEmpty = currentFit.empty();
	this->px_count = countNonZero(lane_inds);
	if (!isCurrentFitEmpty) {
		if (!this->best.empty()) {
			absdiff(currentFit, this->best, this->diff);
		}

		cout << "Diff: " << this->diff.at<float>(0,2) << " " << this->diff.at<float>(0,1) << " " << this->diff.at<float>(0,0) << endl;

		if ((this->diff.at<float>(0,2) > 0.01 || this->diff.at<float>(0,1) > 0.1 || this->diff.at<float>(0,0) > 10) && fits.size() > 0 ) {
			this->detected = false;
			if (this->fits.size() > 0) {
				this->fits[this->fits.size() - 1].release();
				this->fits.pop_back();
			}

			if (this->fits.size() > 0) {
				this->_cal_best();
			} else {
				this->num_not_found++;
			}
		} else {
			this->detected = true;
			this->fits.push_back(currentFit);
			if (this->fits.size() > 1) {
				this->fits[0].release();
				this->fits.erase(this->fits.begin());
			}
			this->num_not_found = 0;
			this->_cal_best();
		}
		//cout<<"Is found: "<<this->detected<<endl;
	} else {
		this->detected = false;
		if (this->fits.size() > 0) {
			this->fits[this->fits.size() - 1].release();
			this->fits.pop_back();
		}

		if (this->fits.size() > 0) {
			this->_cal_best();
		} else {
			this->num_not_found++;
		}
	}

	if (this->num_not_found == 1) {
		
		if (!isCurrentFitEmpty) {
			
			/*	
			if (this->diff.at<float>(0,1) > 1 && this->diff.at<float>(0,0) > 50) {
				this->release();
				return;
			}
			*/
			this->detected = false;
			this->fits.push_back(currentFit);
			this->num_not_found = 0;
			this->_cal_best();
		}
		else {
			this->release();
		}
	

		//this->release();
	}
}

void Lane::release() {
	this->detected = false;
	this->fits.clear();
	this->best.release();
	this->diff.release();
	this->diff = Mat::zeros(1, 3, CV_32FC1);
	this->num_not_found = 0;
}

void Lane::_cal_best() {
	this->best = this->fits[0].clone(); 
	for (int i = 1; i < this->fits.size(); i++) {
		this->best = this->best + this->fits[i];
	}
	divide(this->best, this->fits.size(), this->best);
}

void Lane::_sliding_window_polyfit(const Mat& img, Mat& currentFit, std::vector<int> &lane_inds) {
	//----------------Get histogram of half bottom of image----------------
	Mat histogram;
	//reduce(img(Range((this->img_h / 2), this->img_h), Range::all()), histogram, 0, CV_REDUCE_SUM, CV_32SC1);
	reduce(img, histogram, 0, CV_REDUCE_SUM, CV_32SC1);

	//---------Get the point of max value of right half or left half of the histogram
	int midPoint = (int) (this->img_w / 2), quarter_point = (int) (this->img_w / 4);
	//cout <<midPoint <<" "<<quarter_point<<" "<<histogram.cols<<" "<<histogram.rows<<endl;

	int x_base;
	Point point_temp;

	if (isLeft) {
		minMaxLoc(histogram.colRange(0, midPoint), NULL, NULL, NULL, &point_temp);
		x_base = point_temp.x;
		//x_base += quarter_point;
	} else {
		minMaxLoc(histogram.colRange(midPoint, this->img_w), NULL, NULL, NULL, &point_temp);
		x_base = point_temp.x;
		x_base += midPoint;
	}



	//---------Remove the histogram-----------
	histogram.release();


	//---------Find the position of the white point in the image ---------------
	std::vector<Point> idx;
	int nwindows = 10, window_height = (int) (this->img_h / nwindows);
	std::vector<int> nonzerox;
	std::vector<int> nonzeroy;

	findNonZero (img, idx);

	if (idx.size() == 0) return;

	_nonzero_idx_to_vector(idx, &nonzerox, &nonzeroy);
	// cout << nonzerox.size() << " " << nonzeroy.size() << endl;

	idx.clear();

	//cout << nonzerox.size() << " " << nonzeroy.size() << endl;
	int x_current = x_base;

	Mat temp1, temp2, temp3;
	std::vector<int> good_inds;
	for (int i = 0; i < nwindows - 2; i++) {

		//============ Define boundary for point ===========
		int win_y_low = this->img_h - (i + 1) * window_height,
			win_y_high = this->img_h - i * window_height,
			win_x_low, win_x_high;

			win_x_low = x_current - margin;
			win_x_high = x_current + margin;
			//cout<<x_current <<" "<< win_x_low<< " " <<win_x_high<<endl;


		
		//========== Find point inside the 
		inRange(nonzeroy, Scalar(win_y_low), Scalar(win_y_high), temp1);
		inRange(nonzerox, Scalar(win_x_low), Scalar(win_x_high), temp2);

		
		bitwise_and(temp1, temp2, temp3);
		findNonZero(temp3, idx);
		_nonzero_idx_to_vector(idx, &good_inds, NULL);
		//cout << "Good inds: " << good_inds.size() << endl;

		idx.clear();
		temp1.release(); temp2.release(); temp3.release();


		//cout << good_right_inds.size() << " " << good_left_inds.size() << endl;


		if (good_inds.size() > minpix) {
			x_current =	mean(_sub_vector(nonzerox, good_inds))[0];
		}

		lane_inds.reserve(good_inds.size());
		lane_inds.insert(lane_inds.end(), good_inds.begin(), good_inds.end());

		good_inds.clear();

		//cout << lane_inds.size() << endl;
	}


	if (lane_inds.size() < 50) return;

	std::vector<float> x;
	std::vector<float> y;

	x = _sub_vector(nonzerox, lane_inds);
	y = _sub_vector(nonzeroy, lane_inds);


	Mat temp_x = Mat(x.size(), 1, CV_32FC1, x.data());
	Mat temp_y = Mat(y.size(), 1, CV_32FC1, y.data());
	currentFit = Mat(3, 1, CV_32FC1);


	if (x.size() != 0) {
		_polyfit(temp_y, temp_x, currentFit, 2);
	}



	x.clear(); y.clear();
	temp_x.release(); temp_y.release();
}

void Lane::_slidding_window_use_prev_fit(const Mat& img, const Mat& fit_prev, Mat& currentFit, std::vector<int> &lane_inds) {
	
	std::vector<Point> idx;

	findNonZero(img, idx);

	std::vector<int> nonzerox;
	std::vector<int> nonzeroy;

	_nonzero_idx_to_vector(idx, &nonzerox, &nonzeroy);

	idx.clear();



	_find_next_land_inds_using_prev_fit(nonzerox, nonzeroy, fit_prev, lane_inds);

	if (lane_inds.size() < 50) return;

	std::vector<float> x;
	std::vector<float> y;

	x = _sub_vector(nonzerox, lane_inds);
	y = _sub_vector(nonzeroy, lane_inds);

	Mat temp_x = Mat(x.size(), 1, CV_32FC1, x.data());
	Mat temp_y = Mat(y.size(), 1, CV_32FC1, y.data());
	currentFit = Mat(3, 1, CV_32FC1);

	if (x.size() != 0) {
		_polyfit(temp_y, temp_x, currentFit, 2);
	}



	x.clear();
	y.clear();
	temp_x.release();
	temp_y.release();
}

void Lane::_polyfit(const Mat& src_x, const Mat& src_y, Mat& dst, int order) {
	CV_Assert((src_x.rows>0)&&(src_y.rows>0)&&(src_x.cols==1)&&(src_y.cols==1)
            &&(dst.cols==1)&&(dst.rows==(order+1))&&(order>=1));
    Mat X;
    X = Mat::zeros(src_x.rows, order+1,CV_32FC1);
    Mat copy;
    for(int i = 0; i <=order;i++)
    {
        copy = src_x.clone();
        pow(copy,i,copy);
        Mat M1 = X.col(i);
        copy.col(0).copyTo(M1);
    }
    Mat X_t, X_inv;
    transpose(X,X_t);
    Mat temp = X_t*X;
    Mat temp2;
    invert (temp,temp2);
    Mat temp3 = temp2*X_t;
    Mat W = temp3*src_y;
    W.copyTo(dst);
}

void Lane::_find_next_land_inds_using_prev_fit(const std::vector<int> &nonzerox, const std::vector<int> &nonzeroy, const Mat &prevfit, std::vector<int> &lane_inds) {

	for (int i = 0; i < nonzerox.size(); i++) {
		float y = nonzeroy[i], x = nonzerox[i];

		float temp = prevfit.at<float>(0, 2) * y * y + prevfit.at<float>(0, 1) * y + prevfit.at<float>(0, 0);


		if (x > (temp-margin) && x < (temp + margin)) {
			lane_inds.push_back(i);
		}
	} 
}

void Lane::_nonzero_idx_to_vector(const std::vector<Point> idx, std::vector<int> *x, std::vector<int> *y) {
	for (int i = 0; i < idx.size(); i++) {
		if (x) x->push_back(idx[i].x);
		if (y) y->push_back(idx[i].y);
	}
}

void Lane::_nonzero_idx_to_mat(const std::vector<Point> &idx, Mat *x, Mat *y) {
	for (int i = 0; i < idx.size(); i++) {
		if (x) x->at<int>(0, i) = idx[i].x;
		if (y) y->at<int>(0, i) = idx[i].y;
	}
}

std::vector<float> Lane::_sub_vector(const std::vector<int> &main, const std::vector<int> &idx) {
	std::vector<float> rs;
	for (int i = 0; i < idx.size(); i++) {
		int id = idx[i];
		int value = main[id];
		rs.push_back(value);
	}
	return rs;
}

int Lane::_vector_mean(const std::vector<float> &v) {
	float mean = 0, size = v.size();
	for (int i = 0; i < size; i++) {
		float value = v[i];
		mean += value;
	}
	return int (mean / size);
}




