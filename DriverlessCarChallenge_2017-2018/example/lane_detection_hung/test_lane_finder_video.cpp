#include "laneFinder.h"

#define VIDEO_FRAME_WIDTH 640
#define VIDEO_FRAME_HEIGHT 480

int main(int argc, char** argv) {
    VideoCapture cap(argv[1]);

    if(!cap.isOpened()) return -1;

    Mat colorImg;

    LaneFinder laneFinder = new LaneFinder(true);

    while (true) {
        Point center_point(0,0);
        cap >> colorImg;

        if (colorImg.empty()) break;

        resize(colorImg, colorImg, Size(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT));    

        Mat temp = colorImg.clone();

        imshow("Origin", temp);

        cout<<"Theta:"<<laneFinder.findTheta(temp)<<endl;

        if(waitKey(30) >= 0) break;
    }
}