#include "lane.h"
#include <math.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"
#include <chrono>
#include <thread>

#include <vector>

class LaneFinder {
	public:
		bool isTest;
		Lane *rightLane;
		Lane *leftLane;
		Mat M;
		Mat invM;
		Mat center_poly;
		LaneFinder(bool isTest=false);
		~LaneFinder();
		void init();
		double findTheta(const Mat& img, bool is_turn_left = false, bool is_turn_right = false, int *is_obs_left = 0, int *is_obs_right = 0, const vector<vector<Point>> &squares = vector<vector<Point>>());
	private:
		int transformed_h, transformed_w;
		double old_theta;
		double top_y = 0, bottom_y = 150, center_y = 25;

		void _get_perspective_transform_ver1();
		void _get_perspective_transform_ver2();

		void _transform_perspective_ver1(const Mat& src, Mat& dst);
		void _transform_perspective_ver2(const Mat& src, Mat& dst);

		void _binary_image_before_ver1(const Mat& src, Mat& dst);

		void _binary_image_after_ver1(const Mat& src, Mat& dst);
		void _binary_image_after_ver2(const Mat& src, Mat& dst);

		void _draw_poly(Mat& img, const Mat& poly);
		void _find_center();
		float _find_theta(double dx, double dy);
		Mat _remOutlier(const cv::Mat &gray);
		void _abnormal_line(double offset, double *theta, bool *isAbnormal);
};


